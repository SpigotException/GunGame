package de.spigotexception.gungame.commands;

import de.spigotexception.gungame.GunGame;
import de.spigotexception.gungame.util.GunGameLevel;
import de.spigotexception.gungame.util.InventoryStringDeSerializer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GunGameCommand implements CommandExecutor{

    //TODO: update messages with messages from messages.yml (+ maybe level override confirmation)

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.translateAlternateColorCodes('&', (String) GunGame.getInstance().getMessagesCache().get("Errors.commandOnlyAsPlayer")));
            return true;
        }
        Player player = (Player) commandSender;
        if(!player.hasPermission("gungame.command.gungame")) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String) GunGame.getInstance().getMessagesCache().get("Errors.noPermission")));
            return true;
        }

        if(args.length == 0) {
            player.sendMessage(GunGame.getInstance().getPrefix() + "§6GunGame v" + GunGame.getInstance().getDescription().getVersion() + " by SpigotException c:");
            player.sendMessage(GunGame.getInstance().getPrefix() + "§7-> /gungame help");
        }
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("help")) {
                sendHelp(player);
                return true;
            }
            sendHelp(player);
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("setLocation")) {
                if(args[1].equalsIgnoreCase("Spawn")) {
                    GunGame.getInstance().getDataYAML().set("Location.spawn", player.getLocation());
                    GunGame.getInstance().getDataCache().put("Location.spawn", player.getLocation());
                    player.sendMessage(GunGame.getInstance().getPrefix() + "§7Location set!");
                    return true;
                }
                player.sendMessage(GunGame.getInstance().getPrefix() + "§7Available Locations: §6Lobby");
                return true;
            }
            if(args[0].equalsIgnoreCase("addKit")) {
                if(isInteger(args[1])) {
                    player.sendMessage(GunGame.getInstance().getPrefix() + "Checking inventory...");
                    if(isInventoryEmpty(player)) {
                        player.sendMessage(GunGame.getInstance().getPrefix() + "§cThe inventory cannot be empty!");
                        return true;
                    }
                    if(GunGame.getInstance().getLevels().get(Integer.valueOf(args[1])) != null) {
                        player.sendMessage(GunGame.getInstance().getPrefix() + "§cOverriding existing inventory for level " + args[1]);
                        GunGame.getInstance().getSqLite().update("DELETE FROM level_inventories WHERE level = " + Integer.valueOf(args[1]));
                    }
                    GunGame.getInstance().getSqLite().update("INSERT INTO level_inventories (level, inventory) VALUES (" + Integer.valueOf(args[1]) + ", '" + InventoryStringDeSerializer.InventoryToString(player.getInventory()) + "')");
                    GunGame.getInstance().getLevels().put(Integer.valueOf(args[1]), new GunGameLevel(player.getInventory()));
                } else {
                    player.sendMessage(GunGame.getInstance().getPrefix() + "§c§l" +  args[1] + " §r§cis no integer!");
                }
            }
            sendHelp(player);
            return true;
        }

        return true;
    }

    private void sendHelp(Player player) {
        player.sendMessage(GunGame.getInstance().getPrefix() + "§7-> §6/gungame help");
        player.sendMessage(GunGame.getInstance().getPrefix() + "§7-> §6/gungame setLocation [name]");
        player.sendMessage(GunGame.getInstance().getPrefix() + "§7-> §6/gungame addKit [level]");
    }

    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    private boolean isInventoryEmpty(Player player) {
        for(ItemStack it : player.getInventory().getContents())
        {
            if(it != null) return false;
        }
        return true;
    }

}
