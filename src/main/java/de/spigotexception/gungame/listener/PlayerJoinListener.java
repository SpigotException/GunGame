package de.spigotexception.gungame.listener;

import de.spigotexception.gungame.GunGame;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.setHealth(20.0);
        player.setFoodLevel(20);
        player.setLevel(1);
        GunGame.getInstance().getPlayerCache().handleJoin(player);
        if(GunGame.getInstance().getDataCache().get("Location.spawn") != null)
            player.teleport((Location) GunGame.getInstance().getDataCache().get("Location.spawn"));
        if(GunGame.getInstance().getLevels().get(1) != null)
            GunGame.getInstance().getLevels().get(1).setContent(player);
    }

}
