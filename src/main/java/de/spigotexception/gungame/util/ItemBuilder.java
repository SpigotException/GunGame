package de.spigotexception.gungame.util;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;

/**
 * ItemBuilder for
 * @see ItemStack
 * by SpigotException
 *
 * version 1.4
 */

public class ItemBuilder implements Cloneable{

    //<editor-fold desc="variables">
    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private SkullMeta skullMeta;
    private LeatherArmorMeta leatherArmorMeta;
    //</editor-fold>

    //<editor-fold desc="constructor with ItemStack">
    /**
     * Constructor with params:
     * @param itemStack -> ItemStack for item
     */

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack.clone();
        this.itemMeta = itemStack.getItemMeta();
    }
    //</editor-fold>

    //<editor-fold desc="constructor with material">
    /**
     * Constructor with params:
     * @param material -> Material for item
     */

    public ItemBuilder(Material material) {
        this.itemStack = new ItemStack(material);
        this.itemMeta = itemStack.getItemMeta();
    }
    //</editor-fold>

    //<editor-fold desc="setMaterial with Material">
    /**
     * set material with params:
     * @param material -> material
     * @return this class
     */

    public ItemBuilder setMaterial(Material material) {
        this.itemStack = new ItemStack(material);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setMaterial with ItemStack">
    /**
     * set itemstack of item with params:
     * @param itemStack -> itemstack
     * @return this class
     */

    public ItemBuilder setMaterial(ItemStack itemStack) {
        this.itemStack = itemStack.clone();
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setDisplayName">
    /**
     * set the DisplayName for the item with params:
     * @param name -> name which will be displayed
     * @return this class
     */

    public ItemBuilder setDisplayName(String name) {
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setMetaID">
    /**
     * set the subID/metaID for the item (if needed) with params:
     * @param metaID -> sudID/metaID for item
     * @return this class
     */

    public ItemBuilder setMetaID(byte metaID) {
        itemStack.getData().setData(metaID);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setFlag">
    /**
     * add itemflags to item meta
     * @param itemFlags -> itemflag
     * @return this class
     */

    public ItemBuilder setFlag(ItemFlag... itemFlags) {
        this.itemMeta.addItemFlags(itemFlags);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setAmount">
    /**
     * set the amount of items with params:
     * @param amount -> amount of items
     * @return this class
     */

    public ItemBuilder setAmount(int amount) {
        this.itemStack.setAmount(amount);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setDurability">
    /**
     * set the durability of the item with params:
     * @param durability -> durability to set
     * @return this class
     */

    public ItemBuilder setDurability(short durability) {
        this.itemStack.setDurability(durability);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="addEnchantment">
    /**
     * add enchantment to item with params:
     * @param enchantment -> enchantment for the item
     * @param lvl -> level of enchantment
     * @return this class
     */

    public ItemBuilder addEnchantment(Enchantment enchantment, int lvl) {
        this.itemMeta.addEnchant(enchantment, lvl, false);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="addUnsafeEnchantment">
    /**
     * add unsafe enchantment to item with params:
     * @param enchantment -> enchantment for the item
     * @param lvl -> level of enchantment
     * @return this class
     */

    public ItemBuilder addUnsafeEnchantment(Enchantment enchantment, int lvl) {
        this.itemStack.addUnsafeEnchantment(enchantment, lvl);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="clearEnchantments">
    /**
     * remove all enchantments from the item
     * @return this class
     */

    public ItemBuilder clearEnchantments() {
        this.itemMeta.getEnchants().forEach((enchantment, integer) -> this.itemMeta.removeEnchant(enchantment));
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="removeEnchantment">
    /**
     * remove an enchantment from the item with params:
     * @param enchantment -> enchantment which will be removed
     * @return this class
     */

    public ItemBuilder removeEnchantment(Enchantment enchantment) {
        if(this.itemMeta.getEnchants().containsKey(enchantment))
            this.itemMeta.removeEnchant(enchantment);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setLore with List">
    /**
     * set the lore with an list and params:
     * @param lines -> lines for lore
     * @return this class
     */

    public ItemBuilder setLore(List<String> lines) {
        this.itemMeta.setLore(lines);
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setLore with Array">
    /**
     * set the lore with an array and params:
     * @param lines -> lines for lore
     * @return this class
     */

    public ItemBuilder setLore(String... lines) {
        this.itemMeta.setLore(Arrays.asList(lines));
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="addLore">
    /**
     * add lines to lore
     * @param lines -> lines to add
     * @return this class
     */

    public ItemBuilder addLore(String... lines) {
        this.itemMeta.getLore().addAll(Arrays.asList(lines));
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="resetLore">
    /**
     * clears the lore of the item
     * @return this class
     */

    public ItemBuilder resetLore() {
        this.itemMeta.getLore().clear();
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setSkullOwner">
    /**
     * set the skull texture by player name with params:
     * @param name -> name of player
     * @return this class
     */

    public ItemBuilder setSkullOwner(String name) {
        itemStack.setItemMeta(itemMeta);
        skullMeta = (SkullMeta) itemStack.getItemMeta();
        skullMeta.setOwner(name);
        itemStack.setItemMeta(skullMeta);
        itemMeta = itemStack.getItemMeta();
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setColor">
    /**
     * set the color of leather armor with params:
     * @param color -> color for the armor
     * @return this class
     */

    public ItemBuilder setColor(Color color) {
        itemStack.setItemMeta(itemMeta);
        leatherArmorMeta = (LeatherArmorMeta) itemStack.getItemMeta();
        leatherArmorMeta.setColor(color);
        itemStack.setItemMeta(leatherArmorMeta);
        itemMeta = itemStack.getItemMeta();
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="build as normal itemstack">
    /**
     *
     * @return the final ItemStack
     */
    public ItemStack build() {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
    //</editor-fold>

    //<editor-fold desc="build as skull">
    /**
     * @return the final ItemStack as head
     */
    public ItemStack buildSkull() {
        this.setDurability((short)3);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
    //</editor-fold>

}
