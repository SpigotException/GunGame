package de.spigotexception.gungame.util;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CustomConfiguration {

    private final File file;
    private final FileConfiguration fileConfiguration;

    public CustomConfiguration(File folder, String fileName) {
        this.file = new File(folder, fileName);
        try {
            if (!folder.exists())
                folder.mkdirs();
            if (!file.exists())
                file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.fileConfiguration = YamlConfiguration.loadConfiguration(file);
    }

    public void addDefault(String key, Object value) {
        fileConfiguration.addDefault(key, value);
    }

    public void set(String key, Object value) {
        fileConfiguration.set(key, value);
    }

    public Object getObject(String key) {
        return fileConfiguration.get(key);
    }

    public String getString(String key) {
        return (String) fileConfiguration.get(key);
    }

    public Integer getInt(String key) {
        return (Integer) fileConfiguration.get(key);
    }

    public Boolean getBoolean(String key) {
        return (Boolean) fileConfiguration.get(key);
    }

    public List<?> getList(String key) {
        return (List<?>) fileConfiguration.get(key);
    }

    public <U> List<U> getList(String key, Class<U> clazz) {
        return (List<U>) fileConfiguration.get(key);
    }

    public void save() {
        try {
            fileConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getFile() {
        return file;
    }

    public FileConfiguration getFileConfiguration() {
        return fileConfiguration;
    }
}
