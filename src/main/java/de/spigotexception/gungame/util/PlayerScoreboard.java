package de.spigotexception.gungame.util;

import de.spigotexception.gungame.GunGame;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Arrays;
import java.util.HashMap;

public class PlayerScoreboard {

    private HashMap<Player, Scoreboard> scoreboards;
    private String title;
    private String[] titleSplitted;

    public PlayerScoreboard() {
        scoreboards = new HashMap<>();
        title = ChatColor.translateAlternateColorCodes('&', (String) GunGame.getInstance().getMessagesCache().get("ScoreBoard.title"));
        titleSplitted = title.split("(?<=\\G.{16})");
        if(titleSplitted.length > 3) {
            throw new ArrayIndexOutOfBoundsException("Scoreboard title is to long! [" + title.length() + "/48] -> remove " + (title.length() - 48) + " characters!");
        }
        System.out.println(Arrays.toString(titleSplitted));
    }

    public void setScoreboard(Player player) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("dummy", "gungame");
        Team titleTeam = scoreboard.getTeam("title") != null ? scoreboard.getTeam("title") : scoreboard.registerNewTeam("title");

        if(titleSplitted.length >= 1) {
            titleTeam.setPrefix(titleSplitted[0]);
            System.out.println(">> " + titleSplitted[0]);
        }
        if(titleSplitted.length >= 2) {
            titleTeam.setDisplayName(titleSplitted[1]);
            System.out.println(">> " + titleSplitted[1]);
        }
        if(titleSplitted.length >= 3) {
            titleTeam.setSuffix(titleSplitted[2]);
            System.out.println(">> " + titleSplitted[2]);
        }

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(titleTeam.getPrefix() + (titleTeam.getDisplayName() != null ? titleTeam.getDisplayName() : "") + (titleTeam.getSuffix() != null ? titleTeam.getSuffix() : ""));

        objective.getScore("Test").setScore(0);

        scoreboards.put(player, scoreboard);
        player.setScoreboard(scoreboards.get(player));
    }

    public void update(Player player) {
        Scoreboard scoreboard = scoreboards.get(player);
    }

}
