package de.spigotexception.gungame.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GunGameLevel {

    private Inventory inventory;

    public GunGameLevel(Inventory inventory) {
        this.inventory = inventory;
    }

    public void setContent(Player player) {
        player.getInventory().clear();
        for (ItemStack itemStack : inventory) {
            if (itemStack != null) {
                player.getInventory().addItem(itemStack);
            } else {
                player.getInventory().addItem(new ItemStack(Material.AIR));
            }
        }
    }

}
