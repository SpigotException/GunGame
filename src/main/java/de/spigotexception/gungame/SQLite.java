package de.spigotexception.gungame;

import de.spigotexception.gungame.util.InventoryStringDeSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.sql.*;

public class SQLite {

    private Connection connection;

    public SQLite() {
        try {
            File levelDatabase = new File(GunGame.getInstance().getDataFolder(), "levels.db");
            if(!levelDatabase.exists())
                levelDatabase.createNewFile();
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + levelDatabase.getAbsolutePath());
            update("CREATE TABLE IF NOT EXISTS level_inventories (level INT, inventory TEXT)");
            PreparedStatement preparedStatement = prepareStatement("SELECT * FROM level_inventories WHERE level = 1");
            ResultSet resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()) {
                Inventory inventory = Bukkit.createInventory(null, 36);
                inventory.setItem(0, new ItemStack(Material.WOOD_AXE));
                update("INSERT INTO level_inventories (level, inventory) VALUES (1, '" + InventoryStringDeSerializer.InventoryToString(inventory) + "')");
            }
            resultSet.close();
            preparedStatement.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect() throws SQLException {
        if(connection != null)
            connection.close();
    }

    public void update(String sql) {
        try {
            this.connection.createStatement().execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PreparedStatement prepareStatement(String qry) {
        try {
            return connection.prepareStatement(qry);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
