package de.spigotexception.gungame;

import de.spigotexception.gungame.commands.GunGameCommand;
import de.spigotexception.gungame.listener.PlayerJoinListener;
import de.spigotexception.gungame.util.CustomConfiguration;
import de.spigotexception.gungame.util.GunGameLevel;
import de.spigotexception.gungame.util.InventoryStringDeSerializer;
import de.spigotexception.gungame.util.PlayerScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class GunGame extends JavaPlugin {

    private static GunGame instance;

    public static GunGame getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        prefix = "§7[§6GunGame§7] ";
    }

    private CustomConfiguration configYAML, messagesYAML, dataYAML;
    private HashMap<String, Object> configCache, messagesCache, dataCache;

    private MySQL mysql;
    private SQLite sqLite;
    private PlayerCache playerCache;
    private String prefix;
    private PlayerScoreboard playerScoreboard;

    private HashMap<Integer, GunGameLevel> levels;

    @Override
    public void onEnable() {
        instance = this;
        setupConfigs();
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        this.getCommand("gungame").setExecutor(new GunGameCommand());
        mysql = new MySQL();
        sqLite = new SQLite();
        playerCache = new PlayerCache();
        playerScoreboard = new PlayerScoreboard();
        levels = new HashMap<>();
        setupLevels();
        setupWorld();
    }

    private void setupWorld() {
        if(this.dataCache.get("Location.spawn") == null)
            return;
        World world = ((Location) this.dataCache.get("Location.Spawn")).getWorld();
        world.setTime(6500L);
        for (Entity entity : world.getEntities()) {
            if(entity.getType() == EntityType.PLAYER || ((List<?>) configCache.get("Settings.startup.clear.disabledClearMobs")).contains(entity.getType().name()))
                return;
            entity.remove();
        }
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setGameRuleValue("doFireTick", "false");
        world.setGameRuleValue("doMobSpawning", "false");
        world.setGameRuleValue("keepInventory", "false");
    }

    private void setupLevels() {
        try {
            PreparedStatement preparedStatement = sqLite.prepareStatement("SELECT * FROM level_inventories");
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                levels.put(resultSet.getInt("level"), new GunGameLevel(InventoryStringDeSerializer.StringToInventory(resultSet.getString("inventory"))));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        configYAML.save();
        messagesYAML.save();
        dataYAML.save();
        try {
            mysql.disconnect();
            sqLite.disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setupConfigs() {
        configYAML = new CustomConfiguration(this.getDataFolder(), "config.yml");
        messagesYAML = new CustomConfiguration(this.getDataFolder(), "message.yml");
        dataYAML = new CustomConfiguration(this.getDataFolder(), "data.yml");
        configCache = new HashMap<>();
        messagesCache = new HashMap<>();
        dataCache = new HashMap<>();

        //<editor-fold desc="Configuration">
        configYAML.addDefault("MySQL.host", "localhost");
        configYAML.addDefault("MySQL.username", "username");
        configYAML.addDefault("MySQL.password", "password");
        configYAML.addDefault("MySQL.database", "database");

        configYAML.addDefault("Settings.player.onKill.soundEnabled", true);
        configYAML.addDefault("Settings.player.onKill.sound", "NOTE_BASS_DRUM");
        configYAML.addDefault("Settings.player.onKill.customCommandsEnabled", true);
        configYAML.addDefault("Settings.player.onKill.customCommands", Arrays.asList("say hi %player%", "money give %player% 5"));

        configYAML.addDefault("Settings.player.onDeath.soundEnabled", true);
        configYAML.addDefault("Settings.player.onDeath.sound", "VILLAGER_DEATH");
        configYAML.addDefault("Settings.player.onDeath.customCommandsEnabled", true);
        configYAML.addDefault("Settings.player.onDeath.customCommands", Arrays.asList("say bye %player%", "money take %player% 5"));

        configYAML.addDefault("Settings.startup.clear.disabledClearMobs", Collections.singletonList("ARMOR_STAND"));

        configYAML.getFileConfiguration().options().copyDefaults(true);
        configYAML.save();

        configCache.put("MySQL.host", configYAML.getString("MySQL.host"));
        configCache.put("MySQL.username", configYAML.getString("MySQL.username"));
        configCache.put("MySQL.password", configYAML.getString("MySQL.password"));
        configCache.put("MySQL.database", configYAML.getString("MySQL.database"));
        configCache.put("Settings.player.onKill.soundEnabled", configYAML.getBoolean("Settings.player.onKill.soundEnabled"));
        configCache.put("Settings.player.onKill.sound", configYAML.getString("Settings.player.onKill.sound"));
        configCache.put("Settings.player.onKill.customCommandsEnabled", configYAML.getBoolean("Settings.player.onKill.customCommandsEnabled"));
        configCache.put("Settings.player.onKill.customCommands", configYAML.getList("Settings.player.onKill.customCommands", String.class));

        configCache.put("Settings.player.onDeath.soundEnabled", configYAML.getBoolean("Settings.player.onDeath.soundEnabled"));
        configCache.put("Settings.player.onDeath.sound", configYAML.getString("Settings.player.onDeath.sound"));
        configCache.put("Settings.player.onDeath.customCommandsEnabled", configYAML.getBoolean("Settings.player.onDeath.customCommandsEnabled"));
        configCache.put("Settings.player.onDeath.customCommands", configYAML.getList("Settings.player.onDeath.customCommands", String.class));

        configCache.put("Settings.startup.clear.disabledClearMobs", configYAML.getList("Settings.startup.clear.disabledClearMobs", String.class));
        //</editor-fold>

        messagesYAML.addDefault("prefix", "&7[&6GunGame&7]");
        messagesYAML.addDefault("Errors.mysqlconnect", "&cError while connecting to MySQL Server / Database (-> server shutdown) \n -> %error%");
        messagesYAML.addDefault("Errors.mysqlupdate", "&cError while sending MySQL update \n -> %error%");
        messagesYAML.addDefault("Errors.preparestatement", "&cError while preparing MySQL statement \n -> %error%");
        messagesYAML.addDefault("Errors.commandOnlyAsPlayer", "&cThe command can only be executed by a player!");
        messagesYAML.addDefault("Errors.noPermission", "&cYou have no permission for that!");
        messagesYAML.addDefault("ScoreBoard.title", "&6GunGame &8| &7Great Game!");
        messagesYAML.getFileConfiguration().options().copyDefaults(true);
        messagesYAML.save();

        messagesCache.put("Errors.mysqlconnect", messagesYAML.getString("Errors.mysqlconnect"));
        messagesCache.put("Errors.mysqlupdate", messagesYAML.getString("Errors.mysqlupdate"));
        messagesCache.put("Errors.preparestatement", messagesYAML.getString("Errors.preparestatement"));
        messagesCache.put("Errors.commandOnlyAsPlayer", messagesYAML.getString("Errors.commandOnlyAsPlayer"));
        messagesCache.put("Errors.noPermission", messagesYAML.getString("Errors.noPermission"));
        messagesCache.put("ScoreBoard.title", messagesYAML.getString("ScoreBoard.title"));


        if (dataYAML.getObject("Location.spawn") != null)
            dataCache.put("Location.spawn", dataYAML.getObject("Location.spawn"));

        this.prefix = ChatColor.translateAlternateColorCodes('&', messagesYAML.getString("prefix")) + " ";
    }

    public HashMap<String, Object> getConfigCache() {
        return configCache;
    }

    public HashMap<String, Object> getMessagesCache() {
        return messagesCache;
    }

    public MySQL getMysql() {
        return mysql;
    }

    public PlayerCache getPlayerCache() {
        return playerCache;
    }

    public String getPrefix() {
        return prefix;
    }

    public PlayerScoreboard getPlayerScoreboard() {
        return playerScoreboard;
    }

    public CustomConfiguration getDataYAML() {
        return dataYAML;
    }

    public HashMap<String, Object> getDataCache() {
        return dataCache;
    }

    public HashMap<Integer, GunGameLevel> getLevels() {
        return levels;
    }

    public SQLite getSqLite() {
        return sqLite;
    }
}
