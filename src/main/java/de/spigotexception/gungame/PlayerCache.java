package de.spigotexception.gungame;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PlayerCache {

    private HashMap<Player, GungamePlayer> gungamePlayers;

    public PlayerCache() {
        this.gungamePlayers = new HashMap<>();
    }

    public void handleJoin(Player player) {
        gungamePlayers.put(player, new GungamePlayer(player));
    }

    public void handleQuit(Player player) {
        gungamePlayers.get(player).shutdown();
        gungamePlayers.remove(player);
    }

    public class GungamePlayer {

        private final Player player;
        private final String uuid;
        private ExecutorService executorService;
        private int max_level, current_level, kills, deaths;

        public GungamePlayer(Player player) {
            this.player = player;
            this.uuid = player.getUniqueId().toString();
            executorService = Executors.newSingleThreadExecutor();
            sync();
        }

        private void sync() {
            executorService.execute(() -> {
                try {
                    PreparedStatement preparedStatement = GunGame.getInstance().getMysql().prepareStatement("SELECT * FROM gungame_stats WHERE player_uuid = ?");
                    preparedStatement.setString(1, uuid);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    if(resultSet.next()) {
                        setMaxLevel(resultSet.getInt("max_level"));
                        setDeaths(resultSet.getInt("deaths"));
                        setKills(resultSet.getInt("kills"));
                    } else {
                        GunGame.getInstance().getMysql().update("INSERT INTO gungame_stats (player_uuid, max_level, kills, deaths) VALUES ('" + uuid + "', 0, 0, 0)");
                        setMaxLevel(0);
                        setKills(0);
                        setDeaths(0);
                    }
                    setCurrentLevel(0);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            GunGame.getInstance().getPlayerScoreboard().setScoreboard(player);
                        }
                    }.runTask(GunGame.getInstance());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }

        public void shutdown() {
            executorService.shutdown();
        }

        public int getMaxLevel() {
            return max_level;
        }

        public void setMaxLevel(int max_level) {
            this.max_level = max_level;
        }

        public int getCurrentLevel() {
            return current_level;
        }

        public void setCurrentLevel(int current_level) {
            this.current_level = current_level;
        }

        public int getKills() {
            return kills;
        }

        public void setKills(int kills) {
            this.kills = kills;
        }

        public int getDeaths() {
            return deaths;
        }

        public void setDeaths(int deaths) {
            this.deaths = deaths;
        }
    }

}
