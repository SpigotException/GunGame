package de.spigotexception.gungame;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

public class MySQL {

    private Connection connection;

    public MySQL() {
        try {
            String host = (String) GunGame.getInstance().getConfigCache().get("MySQL.host");
            String username = (String) GunGame.getInstance().getConfigCache().get("MySQL.username");
            String password = (String) GunGame.getInstance().getConfigCache().get("MySQL.password");
            String database = (String) GunGame.getInstance().getConfigCache().get("MySQL.database");
            System.out.println("Connection with user " + username + " to database " + database + "...");
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database + "?autoReconnect=true", username, password);
            update("CREATE TABLE IF NOT EXISTS gungame_stats (ID INT NOT NULL AUTO_INCREMENT, player_uuid VARCHAR(36), max_level SMALLINT, kills MEDIUMINT, deaths MEDIUMINT, PRIMARY KEY (ID))");
        } catch (SQLException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', (String) GunGame.getInstance().getMessagesCache().get("Errors.mysqlconnect")).replace("%error%", e.getMessage()));
            Bukkit.shutdown();
        }

    }

    public void update(String sql) {
        try {
            connection.createStatement().execute(sql);
        } catch (SQLException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', (String) GunGame.getInstance().getMessagesCache().get("Errors.mysqlupdate")).replace("%error%", e.getMessage()));
        }
    }

    public PreparedStatement prepareStatement(String query) {
        try {
            return connection.prepareStatement(query);
        } catch (SQLException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', (String) GunGame.getInstance().getMessagesCache().get("Errors.preparestatement")).replace("%error%", e.getMessage()));
            return null;
        }
    }

    public void disconnect() throws SQLException {
        if(connection != null)
            connection.close();
    }

}
